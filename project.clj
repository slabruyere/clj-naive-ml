(defproject clj-naive-ml "0.1.0-SNAPSHOT"
  :description "Naive Machine Learning in Clojure"
  :url "https://www.slabruyere.net/projects/clj-naive-ml"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url  "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [net.mikera/core.matrix "0.62.0"]
                 [clatrix "0.5.0"]
                 [kixi/stats "0.5.2"]
                 [incanter "1.9.3"]
                 [uncomplicate/neanderthal "0.25.6"]
                 [org.clojure/data.csv "0.1.4"]]
  :main ^:skip-aot clj-naive-ml.core
  :source-paths ["src"]
  :target-path "target/%s"
  :resource-paths ["datasets/"]
  :profiles {:uberjar {:aot :all}})


(ns clj-naive-ml.classifier.k-nearest-neighbours
  (:require [clj-naive-ml.classifier.util :as u]))

(defonce DEFAULT_K 5)

(defn- sort-by-second
  [a b]
  (compare (second a) (second b)))

(defn- pick-most-frequent-label
  [labels]
  (->> (frequencies labels)
       (sort sort-by-second)
       (map first)
       last))

(defn fit
  [classifier x-train y-train]
  (cond-> classifier
    (nil? (:k classifier)) (assoc :k DEFAULT_K)
    true                   (assoc :x-train x-train)
    true                   (assoc :y-train y-train)))

(defn predict
  [classifier x-test]
  (for [x x-test]
    (let [distances (map (partial u/distance x) (:x-train classifier))
          indices   (->> (map-indexed vector distances)
                         (sort sort-by-second)
                         (map first)
                         (take (:k classifier)))]
      (->> indices
           (map (partial nth (:y-train classifier)))
           pick-most-frequent-label))))

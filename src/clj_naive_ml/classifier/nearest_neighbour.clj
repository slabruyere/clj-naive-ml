(ns clj-naive-ml.classifier.nearest-neighbour
  (:require [clj-naive-ml.classifier.util :as u]))

(defn fit
  [classifier x-train y-train]
  (-> classifier
      (assoc :x-train x-train)
      (assoc :y-train y-train)))

(defn predict
  [classifier x-test]
  (for [x x-test]
    (let [distances (map (partial u/distance x) (:x-train classifier))
          index     (first (apply min-key second (map-indexed vector distances)))]
      (nth (:y-train classifier) index))))

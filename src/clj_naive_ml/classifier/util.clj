(ns clj-naive-ml.classifier.util)

(defn- square
  [x]
  (* x x))

(defn distance
  [a b]
  (->> (map - a b)
       (map square)
       (reduce (fnil + 0 0))
       Math/sqrt))

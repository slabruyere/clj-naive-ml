(ns clj-naive-ml.util)

(defn split
  [data ratio]
  (let [shuffled (shuffle data)
        ct       (count data)
        n        (Math/round (* ratio ct))]
    {:testing  (take n shuffled)
     :training (take-last (- ct n) shuffled)}))

(defn bool->int
  [b]
  (if b 1 0))

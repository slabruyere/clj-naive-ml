(ns clj-naive-ml.examples.iris
  (:require [clj-naive-ml.datasets :refer [load-iris]]
            [clj-naive-ml.util :as u]
            [clj-naive-ml.classifier.nearest-neighbour :as nn]
            [clj-naive-ml.classifier.k-nearest-neighbours :as knn]))

(defn make-x
  [data]
  (map (juxt :sepal.length :sepal.width :petal.length :petal.width) data))

(defn make-y
  [data]
  (map :variety data))

(defn run-nn
  [& [split-ratio]]
  (let [iris                (load-iris)
        {training :training
         testing  :testing} (u/split iris (or split-ratio 0.3))
        classifier          (nn/fit {}
                                    (make-x training)
                                    (make-y training))
        actual              (make-y testing)
        prediction          (nn/predict classifier (make-x testing))
        nb-good-answers     (->> (map = prediction actual)
                                 (map u/bool->int)
                                 (reduce (fnil + 0 0)))
        accuracy            (float (/ nb-good-answers (count testing)))]
    {:actual     actual
     :prediction prediction
     :accuracy   accuracy}))

(defn run-knn
  [& [k split-ratio]]
  (let [iris                (load-iris)
        {training :training
         testing  :testing} (u/split iris (or split-ratio 0.3))
        classifier          (knn/fit {:k (or k 3)}
                                     (make-x training)
                                     (make-y training))
        actual              (make-y testing)
        prediction          (knn/predict classifier (make-x testing))
        nb-good-answers     (->> (map = prediction actual)
                                 (map u/bool->int)
                                 (reduce (fnil + 0 0)))
        accuracy            (float (/ nb-good-answers (count testing)))]
    {:actual     actual
     :prediction prediction
     :accuracy   accuracy}))


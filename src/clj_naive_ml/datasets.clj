(ns clj-naive-ml.datasets
  (:require [clojure.data.csv :as csv]
            [clojure.java.io :as io]))

(defn clean-fields
  [options m]
  (let [float-fields (:float-fields options)]
    (->> m
         (map (fn [[k v]]
                (if (float-fields k)
                  [k (Float/parseFloat v)]
                  [k v])))
         (into {}))))

(defn csv-data->maps
  [csv-data & [options]]
  (->> (rest csv-data)
       (map zipmap
            (->> (first csv-data) 
                 (map keyword)
                 repeat))
       (map (partial clean-fields options))))

(defn load-iris []
  (csv-data->maps
   (with-open [reader (io/reader (io/resource "iris.csv"))]
     (doall
      (csv/read-csv reader)))
   {:float-fields #{:sepal.length :sepal.width :petal.length :petal.width}}))
